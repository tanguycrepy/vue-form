# Vue - Form

A vue component use to build form.

Inspired by [Formik](https://github.com/jaredpalmer/formik)

## How to use ?

```vue
    <Form
        :initial-value="{
            status: 'new',
            name: 'title'
        }"
        :on-submit="handleSubmit"
    >
        <Field value="new" component="RadioGroup" name="status" label="Status" :choices="[{id: 'new', text: 'New'}, {id: 'done', text: 'Done'}]"/>
        <Field value="title" component="input" name="name" label="name"/>
    </Form>
```

`Formik` component have 2 computed possible value : 

- `initial-value` : the initial values of your form, with their name
- `on-submit` : the function which will be called when your submit your form. The component will call your function with all the data of your form in an object (`this.onSubmit({...this.$data})`)

`Field` component need a `component` props. It can be any input component. 

All props you'll pass to `Field` will be inject to the component you provide. 

To inject custom component like the first `Field` in the exemple, please follow [documentation](https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components)